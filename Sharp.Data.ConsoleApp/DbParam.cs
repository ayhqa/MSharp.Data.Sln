﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSharp.Data
{
    public class DbParam
    {
        private DbParameter param
        {
            get;
            set;
        }

        public DbParam()
        {

        }
        public DbParam(string name, object value)
        {
            this.ParameterName = name;
            this.Value = value;
        }

        public DbParam(string name, object value, DbType dbType)
        {
            this.ParameterName = name;
            this.Value = value;
            this.DbType = dbType;
        }

        public string ParameterName { get; set; }

        public object Value { get; set; }

        public DbType DbType { get; set; }

        public ParameterDirection Direction { get; set; } = ParameterDirection.Input;

        public DbParameter GetParameter(DbProviderFactory dbFactory)
        {
            param = dbFactory.CreateParameter();
            param.ParameterName = ParameterName;
            param.Value = Value;
            param.DbType = DbType;
            param.Direction = Direction;
            return param;
        }
    }
}
