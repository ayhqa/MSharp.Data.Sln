﻿using SharpDB.DDTekOracle;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace SharpDB.DDTekOracle
{
    public partial class OracleDB
    {
        public bool Exists(string strSql, params OracleParam[] cmdParms)
        {
            object obj = QrySingle<object>(strSql, cmdParms);
            int cmdresult;
            if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
            {
                cmdresult = 0;
            }
            else
            {
                cmdresult = int.Parse(obj.ToString());
            }
            if (cmdresult == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
