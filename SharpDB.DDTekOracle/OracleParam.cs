﻿using DDTek.Oracle;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpDB.DDTekOracle
{
    public class OracleParam
    {
        private OracleParameter oraclePara
        {
            get;
            set;
        }

        public OracleParam()
        {

        }
        public OracleParam(string name, object value)
        {
            this.ParameterName = name;
            this.Value = value;
        }

        public OracleParam(string name, object value, DbType dbType)
        {
            this.ParameterName = name;
            this.Value = value;
            this.DbType = dbType;
        }

        public string ParameterName { get; set; }

        public object Value { get; set; }

        public DbType DbType { get; set; }

        public ParameterDirection Direction { get; set; } = ParameterDirection.Input;

        public OracleParameter GetParameter()
        {
            oraclePara = new OracleParameter();
            oraclePara.ParameterName = ParameterName;
            oraclePara.Value = Value;
            oraclePara.DbType = DbType;
            oraclePara.Direction = Direction;
            return oraclePara;
        }


    }
}
