﻿using System;
using System.Diagnostics;

namespace System
{
    public static class StopwatchExtension
    {

        private const string TimeSpan_MS = "毫秒(ms)";
        private const string TimeSpan_S = "秒(s)";
        private const string TimeSpan_MIN = "分(min)";

        #region Action ElapsedMilliseconds

        /// <summary>
        /// 执行forCount次的花费时间(毫秒) 
        /// </summary>
        /// <param name="act"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：毫秒</returns>
        public static string ForWatchMs(this Action act, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                act.Invoke();
            }
            watch.Stop();
            return watch.ElapsedMilliseconds + TimeSpan_MS;
        }

        /// <summary>
        /// 执行forCount次的花费时间(毫秒) 
        /// </summary>
        /// <param name="act"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：毫秒</returns>
        public static string ForWatchMs<T>(this Action<T> act, T t, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                act.Invoke(t);
            }
            watch.Stop();
            return watch.ElapsedMilliseconds + TimeSpan_MS;
        }

        /// <summary>
        /// 执行forCount次的花费时间(毫秒) 
        /// </summary>
        /// <param name="act"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：毫秒</returns>
        public static string ForWatchMs<T1, T2>(this Action<T1, T2> act, T1 t1, T2 t2, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int i = 0; i < forCount; i++)
            {
                act.Invoke(t1, t2);
            }
            watch.Stop();
            return watch.ElapsedMilliseconds + TimeSpan_MS;
        }

        /// <summary>
        /// 执行forCount次的花费时间(毫秒) 
        /// </summary>
        /// <param name="act"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：毫秒</returns>
        public static string ForWatchMs<T1, T2, T3>(this Action<T1, T2, T3> act, T1 t1, T2 t2, T3 t3, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                act.Invoke(t1, t2, t3);
            }
            watch.Stop();
            return watch.ElapsedMilliseconds + TimeSpan_MS;
        }


        /// <summary>
        /// 执行forCount次的花费时间(毫秒) 
        /// </summary>
        /// <param name="act"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：毫秒</returns>
        public static string ForWatchMs<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> act, T1 t1, T2 t2, T3 t3, T4 t4, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                act.Invoke(t1, t2, t3, t4);
            }
            watch.Stop();
            return watch.ElapsedMilliseconds + TimeSpan_MS;
        }

        /// <summary>
        /// 执行forCount次的花费时间(毫秒) 
        /// </summary>
        /// <param name="act"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：毫秒</returns>
        public static string ForWatchMs<T1, T2, T3, T4, T5>(this Action<T1, T2, T3, T4, T5> act, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                act.Invoke(t1, t2, t3, t4, t5);
            }
            watch.Stop();
            return watch.ElapsedMilliseconds + TimeSpan_MS;
        }


        #endregion

        #region Func ElapsedMilliseconds

        /// <summary>
        /// 执行forCount次的花费时间(毫秒) 
        /// </summary>
        /// <param name="fun"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：毫秒</returns>
        public static string ForWatchMs<T>(this Func<T> fun, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                fun.Invoke();
            }
            watch.Stop();
            return watch.ElapsedMilliseconds + TimeSpan_MS;
        }

        /// <summary>
        /// 执行forCount次的花费时间(毫秒) 
        /// </summary>
        /// <param name="fun"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：毫秒</returns>
        public static string ForWatchMs<T1, TResult>(this Func<T1, TResult> fun, T1 t1, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                fun.Invoke(t1);
            }
            watch.Stop();
            return watch.ElapsedMilliseconds + TimeSpan_MS;
        }

        /// <summary>
        /// 执行forCount次的花费时间(毫秒) 
        /// </summary>
        /// <param name="fun"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：毫秒</returns>
        public static string ForWatchMs<T1, T2, TResult>(this Func<T1, T2, TResult> fun, T1 t1, T2 t2, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                fun.Invoke(t1, t2);
            }
            watch.Stop();
            return watch.ElapsedMilliseconds + TimeSpan_MS;
        }


        /// <summary>
        /// 执行forCount次的花费时间(毫秒) 
        /// </summary>
        /// <param name="fun"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：毫秒</returns>
        public static string ForWatchMs<T1, T2, T3, TResult>(this Func<T1, T2, T3, TResult> fun, T1 t1, T2 t2, T3 t3, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                fun.Invoke(t1, t2, t3);
            }
            watch.Stop();
            return watch.ElapsedMilliseconds + TimeSpan_MS;
        }

        /// <summary>
        /// 执行forCount次的花费时间(毫秒) 
        /// </summary>
        /// <param name="fun"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：毫秒</returns>
        public static string ForWatchMs<T1, T2, T3, T4, TResult>(this Func<T1, T2, T3, T4, TResult> fun, T1 t1, T2 t2, T3 t3, T4 t4, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                fun.Invoke(t1, t2, t3, t4);
            }
            watch.Stop();
            return watch.ElapsedMilliseconds + TimeSpan_MS;
        }

        /// <summary>
        /// 执行forCount次的花费时间(毫秒) 
        /// </summary>
        /// <param name="fun"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：毫秒</returns>
        public static string ForWatchMs<T1, T2, T3, T4, T5, TResult>(this Func<T1, T2, T3, T4, T5, TResult> fun, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                fun.Invoke(t1, t2, t3, t4, t5);
            }
            watch.Stop();
            return watch.ElapsedMilliseconds + TimeSpan_MS;
        }



        #endregion

        #region Action Elapsed.TotalSeconds

        /// <summary>
        /// 执行forCount次的花费时间(秒) 
        /// </summary>
        /// <param name="act"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：秒</returns>
        public static string ForWatchSec(this Action act, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                act.Invoke();
            }
            watch.Stop();
            return watch.Elapsed.TotalSeconds + TimeSpan_S;
        }

        /// <summary>
        /// 执行forCount次的花费时间(秒) 
        /// </summary>
        /// <param name="act"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：秒</returns>
        public static string ForWatchSec<T>(this Action<T> act, T t, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                act.Invoke(t);
            }
            watch.Stop();
            return watch.Elapsed.TotalSeconds + TimeSpan_S;
        }

        /// <summary>
        /// 执行forCount次的花费时间(秒) 
        /// </summary>
        /// <param name="act"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：秒</returns>
        public static string ForWatchSec<T1, T2>(this Action<T1, T2> act, T1 t1, T2 t2, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int i = 0; i < forCount; i++)
            {
                act.Invoke(t1, t2);
            }
            watch.Stop();
            return watch.Elapsed.TotalSeconds + TimeSpan_S;
        }

        /// <summary>
        /// 执行forCount次的花费时间(秒) 
        /// </summary>
        /// <param name="act"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：秒</returns>
        public static string ForWatchSec<T1, T2, T3>(this Action<T1, T2, T3> act, T1 t1, T2 t2, T3 t3, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                act.Invoke(t1, t2, t3);
            }
            watch.Stop();
            return watch.Elapsed.TotalSeconds + TimeSpan_S;
        }


        /// <summary>
        /// 执行forCount次的花费时间(秒) 
        /// </summary>
        /// <param name="act"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：秒</returns>
        public static string ForWatchSec<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> act, T1 t1, T2 t2, T3 t3, T4 t4, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                act.Invoke(t1, t2, t3, t4);
            }
            watch.Stop();
            return watch.Elapsed.TotalSeconds + TimeSpan_S;
        }

        /// <summary>
        /// 执行forCount次的花费时间(秒) 
        /// </summary>
        /// <param name="act"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：秒</returns>
        public static string ForWatchSec<T1, T2, T3, T4, T5>(this Action<T1, T2, T3, T4, T5> act, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                act.Invoke(t1, t2, t3, t4, t5);
            }
            watch.Stop();
            return watch.Elapsed.TotalSeconds + TimeSpan_S;
        }


        #endregion

        #region Func Elapsed.TotalSeconds

        /// <summary>
        /// 执行forCount次的花费时间(秒) 
        /// </summary>
        /// <param name="fun"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：秒</returns>
        public static string ForWatchSec<T>(this Func<T> fun, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                fun.Invoke();
            }
            watch.Stop();
            return watch.Elapsed.TotalSeconds + TimeSpan_S;
        }

        /// <summary>
        /// 执行forCount次的花费时间(秒) 
        /// </summary>
        /// <param name="fun"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：秒</returns>
        public static string ForWatchSec<T1, TResult>(this Func<T1, TResult> fun, T1 t1, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                fun.Invoke(t1);
            }
            watch.Stop();
            return watch.Elapsed.TotalSeconds + TimeSpan_S;
        }

        /// <summary>
        /// 执行forCount次的花费时间(秒) 
        /// </summary>
        /// <param name="fun"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：秒</returns>
        public static string ForWatchSec<T1, T2, TResult>(this Func<T1, T2, TResult> fun, T1 t1, T2 t2, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                fun.Invoke(t1, t2);
            }
            watch.Stop();
            return watch.Elapsed.TotalSeconds + TimeSpan_S;
        }


        /// <summary>
        /// 执行forCount次的花费时间(秒) 
        /// </summary>
        /// <param name="fun"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：秒</returns>
        public static string ForWatchSec<T1, T2, T3, TResult>(this Func<T1, T2, T3, TResult> fun, T1 t1, T2 t2, T3 t3, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                fun.Invoke(t1, t2, t3);
            }
            watch.Stop();
            return watch.Elapsed.TotalSeconds + TimeSpan_S;
        }

        /// <summary>
        /// 执行forCount次的花费时间(秒) 
        /// </summary>
        /// <param name="fun"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：秒</returns>
        public static string ForWatchSec<T1, T2, T3, T4, TResult>(this Func<T1, T2, T3, T4, TResult> fun, T1 t1, T2 t2, T3 t3, T4 t4, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                fun.Invoke(t1, t2, t3, t4);
            }
            watch.Stop();
            return watch.Elapsed.TotalSeconds + TimeSpan_S;
        }

        /// <summary>
        /// 执行forCount次的花费时间(秒) 
        /// </summary>
        /// <param name="fun"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：秒</returns>
        public static string ForWatchSec<T1, T2, T3, T4, T5, TResult>(this Func<T1, T2, T3, T4, T5, TResult> fun, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                fun.Invoke(t1, t2, t3, t4, t5);
            }
            watch.Stop();
            return watch.Elapsed.TotalSeconds + TimeSpan_S;
        }



        #endregion

        #region Action Elapsed.TotalMinutes

        /// <summary>
        /// 执行forCount次的花费时间(分钟) 
        /// </summary>
        /// <param name="act"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：分钟</returns>
        public static string ForWatchMin(this Action act, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                act.Invoke();
            }
            watch.Stop();
            return watch.Elapsed.TotalMinutes + TimeSpan_MIN;
        }

        /// <summary>
        /// 执行forCount次的花费时间(分钟) 
        /// </summary>
        /// <param name="act"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：分钟</returns>
        public static string ForWatchMin<T>(this Action<T> act, T t, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                act.Invoke(t);
            }
            watch.Stop();
            return watch.Elapsed.TotalMinutes + TimeSpan_MIN;
        }

        /// <summary>
        /// 执行forCount次的花费时间(分钟) 
        /// </summary>
        /// <param name="act"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：分钟</returns>
        public static string ForWatchMin<T1, T2>(this Action<T1, T2> act, T1 t1, T2 t2, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int i = 0; i < forCount; i++)
            {
                act.Invoke(t1, t2);
            }
            watch.Stop();
            return watch.Elapsed.TotalMinutes + TimeSpan_MIN;
        }

        /// <summary>
        /// 执行forCount次的花费时间(分钟) 
        /// </summary>
        /// <param name="act"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：分钟</returns>
        public static string ForWatchMin<T1, T2, T3>(this Action<T1, T2, T3> act, T1 t1, T2 t2, T3 t3, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                act.Invoke(t1, t2, t3);
            }
            watch.Stop();
            return watch.Elapsed.TotalMinutes + TimeSpan_MIN;
        }


        /// <summary>
        /// 执行forCount次的花费时间(分钟) 
        /// </summary>
        /// <param name="act"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：分钟</returns>
        public static string ForWatchMin<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> act, T1 t1, T2 t2, T3 t3, T4 t4, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                act.Invoke(t1, t2, t3, t4);
            }
            watch.Stop();
            return watch.Elapsed.TotalMinutes + TimeSpan_MIN;
        }

        /// <summary>
        /// 执行forCount次的花费时间(分钟) 
        /// </summary>
        /// <param name="act"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：分钟</returns>
        public static string ForWatchMin<T1, T2, T3, T4, T5>(this Action<T1, T2, T3, T4, T5> act, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                act.Invoke(t1, t2, t3, t4, t5);
            }
            watch.Stop();
            return watch.Elapsed.TotalMinutes + TimeSpan_MIN;
        }


        #endregion

        #region Func Elapsed.TotalMinutes

        /// <summary>
        /// 执行forCount次的花费时间(分钟) 
        /// </summary>
        /// <param name="fun"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：分钟</returns>
        public static string ForWatchMin<T>(this Func<T> fun, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                fun.Invoke();
            }
            watch.Stop();
            return watch.Elapsed.TotalMinutes + TimeSpan_MIN;
        }

        /// <summary>
        /// 执行forCount次的花费时间(分钟) 
        /// </summary>
        /// <param name="fun"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：分钟</returns>
        public static string ForWatchMin<T1, TResult>(this Func<T1, TResult> fun, T1 t1, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                fun.Invoke(t1);
            }
            watch.Stop();
            return watch.Elapsed.TotalMinutes + TimeSpan_MIN;
        }

        /// <summary>
        /// 执行forCount次的花费时间(分钟) 
        /// </summary>
        /// <param name="fun"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：分钟</returns>
        public static string ForWatchMin<T1, T2, TResult>(this Func<T1, T2, TResult> fun, T1 t1, T2 t2, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                fun.Invoke(t1, t2);
            }
            watch.Stop();
            return watch.Elapsed.TotalMinutes + TimeSpan_MIN;
        }


        /// <summary>
        /// 执行forCount次的花费时间(分钟) 
        /// </summary>
        /// <param name="fun"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：分钟</returns>
        public static string ForWatchMin<T1, T2, T3, TResult>(this Func<T1, T2, T3, TResult> fun, T1 t1, T2 t2, T3 t3, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                fun.Invoke(t1, t2, t3);
            }
            watch.Stop();
            return watch.Elapsed.TotalMinutes + TimeSpan_MIN;
        }

        /// <summary>
        /// 执行forCount次的花费时间(分钟) 
        /// </summary>
        /// <param name="fun"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：分钟</returns>
        public static string ForWatchMin<T1, T2, T3, T4, TResult>(this Func<T1, T2, T3, T4, TResult> fun, T1 t1, T2 t2, T3 t3, T4 t4, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                fun.Invoke(t1, t2, t3, t4);
            }
            watch.Stop();
            return watch.Elapsed.TotalMinutes + TimeSpan_MIN;
        }

        /// <summary>
        /// 执行forCount次的花费时间(分钟) 
        /// </summary>
        /// <param name="fun"></param>
        /// <param name="forCount">执行次数</param>
        /// <returns>花费时间：分钟</returns>
        public static string ForWatchMin<T1, T2, T3, T4, T5, TResult>(this Func<T1, T2, T3, T4, T5, TResult> fun, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, long forCount)
        {
            if (forCount < 1) throw new ArgumentException("forCount", "forCount至少为1！");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int j = 1; j <= forCount; j++)
            {
                fun.Invoke(t1, t2, t3, t4, t5);
            }
            watch.Stop();
            return watch.Elapsed.TotalMinutes + TimeSpan_MIN;
        }



        #endregion
    }
}
