﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace MSharp.Data.UnitTestProject
{
	/// <summary>
	/// GPS信息表
	/// </summary>
	[DisplayName("GPS信息表")]
	[Serializable]
	public partial class GPSINFORMATION
	{
		public GPSINFORMATION()
		{
		}

		/// <summary>
		/// 经度
		/// </summary>
		[DisplayName("经度")]
		public string LONGITUDE { get;set; }

		/// <summary>
		/// 纬度
		/// </summary>
		[DisplayName("纬度")]
		public string LATITUDE { get;set; }

		/// <summary>
		/// 转换后百度地图的经度
		/// </summary>
		[DisplayName("转换后百度地图的经度")]
		public string BMAPLONGITUDE { get;set; }

		/// <summary>
		/// 转换后百度地图的纬度
		/// </summary>
		[DisplayName("转换后百度地图的纬度")]
		public string BMAPLATITUDE { get;set; }

		/// <summary>
		/// 转换后百度地图的地址
		/// </summary>
		[DisplayName("转换后百度地图的地址")]
		public string BMAPADDRESS { get;set; }

		/// <summary>
		/// 定位时间
		/// </summary>
		[DisplayName("定位时间")]
		public DateTime? GPSTIME { get;set; }

		/// <summary>
		/// 创建时间
		/// </summary>
		[DisplayName("创建时间")]
		public DateTime? CREATETIME { get;set; }

		/// <summary>
		/// 标识符（标准版：GPS/LBS）
		/// </summary>
		[DisplayName("标识符（标准版：GPS/LBS）")]
		public string TOKEN { get;set; }

		/// <summary>
		/// 系统ID
		/// </summary>
		[DisplayName("系统ID")]
		public string GPSID { get;set; }

		/// <summary>
		/// ENGID
		/// </summary>
		[DisplayName("ENGID")]
		public string ENGID { get;set; }

        /// <summary>
        /// LBS数据来源
        /// </summary>
        [DisplayName("LBS数据来源")]
        public string LBS_SOURCE
        {
            get;
            set;
        }

        /// <summary>
        /// 加盐小数
        /// </summary>
        [DisplayName("百度纬度 加盐小数")]
        public string BD_LAT_SALT { get; set; }
	}
}
