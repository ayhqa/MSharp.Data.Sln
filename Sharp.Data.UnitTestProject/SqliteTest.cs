﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSharp.Data.UnitTestProject
{
    [TestClass]
    public class SqliteTest
    {
        public static DB db = DBMgr.Connect(DBType.SQLite, @"D:\1.db");

        static SqliteTest()
        {
            db.OnExecuting = (sql, pars) =>
            {
                var arrPar = pars.ToArray();
            };

            db.OnError = (sql, pars, exp) =>
            {
                var arrPar = pars.ToArray();
            };

            db.OnExecutingChange = (sql, pars) =>
            {
                return new KeyValuePair<string, DbParameter[]>(sql, pars.ToArray());
            };

            db.OnExecuted = (sql, pars, res) =>
            {
                var arrPar = pars.ToArray();
            };
        }

        [TestMethod]
        public void TestMethod()
        {
            bool bl1 = db.Insert(new { Name = "xiaoqiang", Age = 20 }, "Person");

            int in1 = db.InsertGetInt(new { Name = "xiaoqiang1", Age = 20 }, "Person");

            long lg1 = db.InsertGetLong(new { Name = "xiaoqiang2", Age = 20 }, "Person");
        }



        [TestMethod]
        public void TestMethod3()
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc.Add("name", "xiaomeng" + new Random().Next());
            nvc.Add("Age", "30");
            nvc.Add("id", "1");

            bool bl1 = db.Update(nvc, "Person");

            bool bl2 = db.Update(new { name = "xiaoxiao", Id = "2" }, "Person");

            Dictionary<string, object> dict = new Dictionary<string, object>();
            dict.Add("name", "ABCDEF");
            dict.Add("Id", "3");

            bool bl3 = db.Update(dict, "Person");
        }
    }
}
