﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSharp.Data.SPI
{
    public interface IDBInfo
    {
        /// <summary>
        /// 当前数据库名称
        /// </summary>
        string DBName { get; }

        /// <summary>
        /// 刷新表结构(重新查询)
        /// </summary>
        /// <returns></returns>
        bool Refresh();

        /// <summary>
        /// 获取数据库中所有表名称
        /// </summary>
        List<string> TableNames { get; }


        NameValueCollection TableComments { get; }

        
        Dictionary<string, TableInfo> TableInfoDict { get; }


        Dictionary<string, List<string>> TableColumnNameDict { get; }


        Dictionary<string, List<ColumnInfo>> TableColumnInfoDict { get; }


        Dictionary<string, NameValueCollection> TableColumnComments { get; }


        /// <summary>
        /// 获取所有数据库名称
        /// </summary>
        [Obsolete("注意：由于Oralce没有库名,只有表空间,所以Oracle没有提供数据库名称查询支持。", false)]
        List<string> DBNames { get; }

        /// <summary>
        /// 获取当前表的所有列名称
        /// </summary>
        /// <param name="tableName">数据库表名称</param>
        /// <returns></returns>
        List<string> this[string tableName] { get; }

        /// <summary>
        /// 获取当前表的列的列详细信息
        /// </summary>
        /// <param name="tableName">数据库表名称</param>
        /// <param name="columnName">列名</param>
        /// <returns></returns>
        ColumnInfo this[string tableName, string columnName] { get; }
        
        /// <summary>
        /// 检查表是否已经存在
        /// </summary>
        /// <param name="tableName">数据库表名称</param>
        /// <returns></returns>
        bool IsExistTable(string tableName);

        /// <summary>
        /// 检查表的某一列是否存在
        /// </summary>
        /// <param name="tableName">数据库表名称</param>
        /// <param name="columnName">列名称</param>
        /// <returns></returns>
        bool IsExistColumn(string tableName, string columnName);

        /// <summary>
        /// 获取表的列的描述
        /// </summary>
        /// <param name="tableName">数据库表名称</param>
        /// <param name="columnName">列名称</param>
        /// <returns></returns>
        string GetColumnComment(string tableName, string columnName);


        /// <summary>
        /// 获取当前表在数据库的描述
        /// </summary>
        /// <param name="tableName">数据库表名称</param>
        /// <returns></returns>
        string GetTableComment(string tableName);

        /// <summary>
        /// 获取当前表的所有列详细信息
        /// </summary>
        /// <param name="tableName">数据库表名称</param>
        /// <returns></returns>
        List<ColumnInfo> GetColumns(string tableName);

        /// <summary>
        /// 更新表的描述
        /// </summary>
        /// <param name="tableName">数据库表名称</param>
        /// <param name="comment">表描述</param>
        /// <returns></returns>
        bool SetTableComment(string tableName, string comment);

        /// <summary>
        /// 更新表的列的描述
        /// 注意：MySql修改列描述，比较费事，如果数据量较大，操作会比较耗时！详细介绍：http://blog.sina.com.cn/s/blog_72aace390102uwgg.html
        /// </summary>
        /// <param name="tableName">数据库表名称</param>
        /// <param name="columnName">列名称</param>
        /// <param name="comment">列描述</param>
        /// <returns></returns>
        bool SetColumnComment(string tableName, string columnName, string comment);

        /// <summary>
        /// Drop数据中的某张表
        /// </summary>
        /// <param name="tableName">数据库表名称</param>
        /// <returns></returns>
        bool DropTable(string tableName);

        /// <summary>
        /// 修改表结构，drop 某张表的某一列
        /// </summary>
        /// <param name="tableName">数据库表名称</param>
        /// <param name="columnName">列名称</param>
        /// <returns></returns>
        bool DropColumn(string tableName, string columnName);

        /// <summary>
        /// 获取当前数据库 表名称及最后更改时间
        ///注意：只针对SqlServer/Oracle/PostgreSql/MySql。MySql的所有表存储引擎必须为MyISAM 方才查询支持，方才查询准确。
        /// </summary>
        /// <returns></returns>
        [Obsolete("注意：MySql的所有表存储引擎必须为MyISAM 方才查询支持，方才查询准确。", false)]
        Dictionary<string, DateTime> GetTableStruct_Modify();


        
    }
}
