﻿using DDTek.Oracle;
using MySql.Data.MySqlClient;
using Npgsql;
using Oracle.ManagedDataAccess.Client;
using MSharp.Data.Database;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MSharp.Data
{
    internal class DBFactory
    {
        public static DB CreateInstance(DBType dbType, string connectionString)
        {
            switch (dbType)
            {
                case DBType.SqlServer:
                    return new SqlServerDB(dbType, SqlClientFactory.Instance, connectionString);
                case DBType.MySql:
                    return new MySqlDB(dbType, MySqlClientFactory.Instance, connectionString);
                case DBType.Oracle:
                    return new OracleDB(dbType, OracleClientFactory.Instance, connectionString);
                case DBType.OracleDDTek:
                    return new OracleDDTekDB(dbType, OracleFactory.Instance, connectionString);
                case DBType.PostgreSql:
                    return new PostgreSqlDB(dbType, NpgsqlFactory.Instance, connectionString);
                case DBType.SQLite:
                    return new SQLiteDB(dbType, SQLiteFactory.Instance, connectionString);
                default:
                    throw new ArgumentException("未支持的数据库类型！");
            }
        }
    }
}
